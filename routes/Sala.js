const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const cors = require('./cors');
const Salas = require('../models/Sala');

const SalaRouter = express.Router();
SalaRouter.use(bodyParser.json());

SalaRouter.route('/').
options(cors.corsWithOptions, (req, res) => { res.sendStatus(200); })
    .get(cors.cors, (req, res, next) => {
        Salas.find({}).sort({Nombre: -1})
            .then((salas) => {
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                res.json(salas);
            }, (err) => next(err))
            .catch((err) => next(err));
    })
    .post(cors.corsWithOptions,
        (req, res, next) => {
        Salas.create(req.body)
            .then((Sala) => {
                console.log('Sala Created', Sala);
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                res.json(Sala);

            }, (err) => next(err))
            .catch((err) => next(err));
    })
    .put(cors.corsWithOptions, 
        (req, res, next) => {
        res.statusCode = 403;
        res.end('La operacion PUT no es soportada para esta ruta');
    })
    .delete(cors.corsWithOptions, 
        (req, res, next) => {
        res.statusCode = 403;
        res.end('La operacion DELETE no es soportada para esta ruta');
    });

SalaRouter.route('/:salaId').
options(cors.corsWithOptions, (req, res) => { res.sendStatus(200); })
    .get(cors.cors, (req, res, next) => {
        Salas.findById(req.params.salaId)
            .then((salas) => {
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                res.json(salas);
            }, (err) => next(err))
            .catch((err) => next(err));
    })
    .post(cors.corsWithOptions, 
        (req, res, next) => {

    })
    .put(cors.corsWithOptions, 
        (req, res, next) => {
        Salas.findByIdAndUpdate(req.params.salaId, {
            $set: req.body
        }, { new: true })
            .then((sala) => {
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                res.json(sala);
            }, (err) => next(err))
            .catch((err) => next(err));
    })
    .delete(cors.corsWithOptions, 
        (req, res, next) => {
        Salas.findByIdAndRemove(req.params.salaId)
            .then((resp) => {
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                res.json(resp);
            }, (err) => next(err))
            .catch((err) => next(err));
    });



module.exports = SalaRouter;