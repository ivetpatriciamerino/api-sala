var superagent = require('superagent')
var expect = require('expect.js')

describe('Test Sala', function () {
  var id
  var url = 'http://localhost:3030';


  // ------------------Pruebas routes Sala------------------

  // POST / Sala
  it('Test POST /Sala ', function (done) {
    superagent.post(url + '/Sala')
      .send({
        Fecha_Creacion:"junio",
        Sala_Numero:"701",
        Piso:"7",
        Tipo_Equipos:"Dell"
      })
      .end(function (e, res) {
        id = res.body._id;
       
        expect(e).to.eql(null)
        //expect(res.body.status).to.eql('created')
        expect(res.body._id).to.be.an('string')
        done()
      })
  })
  // GET /Sala
  it('Test GET /Sala ', function (done) {
    superagent.get(url + '/Sala')
      .end(function (e, res) {

        expect(e).to.eql(null)
        expect(typeof res.body).to.eql('object')
        done()
      })
  })

 // GET /Sala/:id
 it('test GET /Sala/:id ', function (done) {
  superagent.get(url + '/Sala/'+id)
        .end(function (e, res) {
         //expect(e).to.eql(null)
        expect(typeof res.body).to.eql('object')
        expect(res.body._id).to.eql(id)
        done()
    })
})
// PUT /Sala/:id
it('Test PUT /Sala/:id ', function (done) {
  console.log("el id es: "+id)
  superagent.put(url + '/Sala/'+id)
    .send({
      Fecha_Creacion:"junio",
      Sala_Numero:"701",
      Piso:"7",
      Tipo_Equipos:"HP"
    })
    .end(function (e, res) {
      //expect(e).to.eql(null)
      //expect(res.body.status).to.eql('updated')
      console.log(res.body)
      expect(res.body.Tipo_Equipos).to.eql('HP')
      done()
    })
})

// DELETE /Sala/:id
it('Test DELETE /Sala/:id ', function (done) {
  superagent.delete(url +'/Sala/'+id)
    .end(function (e, res) {
      //expect(e).to.eql(null)
      expect(typeof res.body).to.eql('object')
      //expect(res.body.status).to.eql('deleted')
      done()
    })
})

})
