var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

const bodyParser =require('body-parser');
const mongoose = require('mongoose');
mongoose.Promise = require('bluebird');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');

//Se procede  a inhabilitar las rutas de los modelos Tipo, Ubicación y Sede.
//Se deja solo la Ruta Sala para ser Usada en el microservicio.

//var TipoRouter = require('./routes/Tipo');// se agregan las rutas
//var UbicacionRouter = require('./routes/Ubicacion');// Se agragn las rutas
//var SedeRouter = require('./routes/Sede');// Se agragn las rutas
var SalaRouter = require('./routes/Sala');// Se agragn las rutas

//const url = 'mongodb://localhost:27017/Reportes';
//const url = 'mongodb://root:Iv3tm3r1@mongo-service:27017/Reportes?authSource=admin';
const url = process.env.MONGO_APISALAS_URL
console.log(url);
const connect = mongoose.connect(url ,{ useNewUrlParser: true});

connect.then((db)=>{
  console.log('Conectado al servidor');
},(err)=> {console.log(err)});

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);

//Se procede  a inhabilitar las rutas de los modelos Tipo, Ubicación y Sede.
//Se deja solo la Ruta Sala para ser Usada en el microservicio.

//app.use('/Tipo', TipoRouter); // Se Usa la  Ruta
//app.use('/Sede', SedeRouter); // Se usa la Ruta
//app.use('/Ubicacion', UbicacionRouter); // Se usa la Ruta
app.use('/Sala', SalaRouter); // Se usa la Ruta

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
