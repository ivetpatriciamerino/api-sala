const mongoose = require('mongoose');
const Schemma = mongoose.Schema;


const SalaSchema = new Schemma({

    Fecha_Creacion:{
        type: String,
        required: true
    },
    Sala_Numero:{
        type: String,
        required: true
    },
    Piso:{
        type: String,
        required: true
    },
    Tipo_Equipos:{
        type: String,
        required: true
    },
            
}, {
        timestamps: true
    });



var Salas = mongoose.model('Sala', SalaSchema);

module.exports = Salas;